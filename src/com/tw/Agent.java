package com.tw;

import java.io.File;
import java.io.IOException;

public class Agent {
    public void receiveCommand(String command) {

        char[] commands = command.toCharArray();
        for (char c : commands) {
            handleSingleCommand(String.valueOf(c));
        }
    }

    private void handleSingleCommand(String command) {
        if (command.equals("c"))
            checkoutCode();
        else if (command.equals("a"))
            updateCode();
        else if (command.equals("r")) {
            try {
                File file = new File("/Users/twer/IdeaProjects/oo-2/package.war");
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            System.out.println("running test.");
        } else if (command.equals("p"))
            System.out.println("publishing.");
    }

    private static void updateCode() {
        System.out.println("updating code.");
    }

    private static void checkoutCode() {
        System.out.println("checking out code.");
    }
}
