package com.tw;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class AgentTest {

    private ByteArrayOutputStream outContent;
    private PrintStream printStream;
    private Agent agent;

    @Before
    public void setUp(){
        outContent = new ByteArrayOutputStream();
        printStream = new PrintStream(outContent);
        System.setOut(printStream);
        agent = new Agent();
    }

    @Test
    public void shouldPrintRunningTestWhenRecieveR(){
        File file = new File("/Users/twer/IdeaProjects/oo-2/package.war");
        assertThat(file.exists(), is(true));

        agent.receiveCommand("r");
        assertThat(outContent.toString(), is("running test.\n"));
    }

    @Test
    public void shouldPrintCheckOutCodeWhenRecieveC(){
        agent.receiveCommand("c");
        assertThat(outContent.toString(), is("checking out code.\n"));
    }

    @Test
    public void shouldPrintUpdateCodeWhenRecieveA(){
        agent.receiveCommand("a");
        assertThat(outContent.toString(), is("updating code.\n"));
    }

    @Test
    public void shouldPrintPublishWhenRecieveP(){
        agent.receiveCommand("p");
        assertThat(outContent.toString(), is("publishing.\n"));
    }

    @Test
    public void shouldDoLotsOfThingsWhenRecieveArp(){
        agent.receiveCommand("arp");
        assertThat(outContent.toString(), is("updating code.\nrunning test.\npublishing.\n"));
        File file = new File("/Users/twer/IdeaProjects/oo-2/package.war");
        assertThat(file.exists(), is(true));
    }
}
