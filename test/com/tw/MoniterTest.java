package com.tw;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created with IntelliJ IDEA.
 * User: twer
 * Date: 1/23/13
 * Time: 7:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class MoniterTest {

    private ByteArrayOutputStream outContent;
    private PrintStream printStream;
    private Monitor monitor;

    @Before
    public void setUp() {
        outContent = new ByteArrayOutputStream();
        printStream = new PrintStream(outContent);
        System.setOut(printStream);
        monitor = new Monitor();
    }

    @Test
    public void shouldPrintCodeUpdatedWhenReceiveA() {
        monitor.receiveCommand("a");
        assertThat(outContent.toString(), is("code updated."));
    }

}
